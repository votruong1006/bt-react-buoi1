import React, { Component } from "react";

export default class EventHandling extends Component {
  handleLogin = () => {
    console.log("yes");
  };
  handleSayHelloWithName = (username) => {
    console.log("hello", username);
  };
  render() {
    return (
      <div>
        <h2>EventHandling</h2>
        <button onClick={this.handleLogin} className="btn btn-success">
          Login
        </button>
        <br />
        <button
          onClick={() => {
            this.handleSayHelloWithName("alice");
          }}
          className="btn btn-warning"
        >
          Say hello with name
        </button>
      </div>
    );
  }
}
