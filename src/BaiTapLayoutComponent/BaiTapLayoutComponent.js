import React, { Component } from "react";
import Banner from "./Banner";
import ContentItem from "./ContentItem";
import Footer from "./Footer";
import Header from "./Header";

export default class BaiTapLayoutComponent extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <div className="container px-5">
          <div className=" row">
            <div className="col-3 ">
              <ContentItem />
            </div>
            <div className="col-3 ">
              <ContentItem />
            </div>
            <div className="col-3 ">
              <ContentItem />
            </div>
            <div className="col-3 ">
              <ContentItem />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
