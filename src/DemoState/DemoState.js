import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    username: "Alice",
  };
  handleChangeUsername = () => {
    let name;
    if (this.state.username === "bob") {
      name = "Alice";
    } else {
      name = "bob";
    }
    this.setState({ username: name });
  };
  render() {
    return (
      <div>
        <h2>DemoState</h2>
        <h3
          className={
            this.state.username === "bob" ? "text-danger" : "text-success"
          }
        >
          {this.state.username}
        </h3>
        <button onClick={this.handleChangeUsername} className="btn btn-warning">
          Change username
        </button>
      </div>
    );
  }
}
