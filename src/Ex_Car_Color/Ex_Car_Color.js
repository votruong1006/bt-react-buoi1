import React, { Component } from "react";

export default class Ex_Car_Color extends Component {
  state = {
    img_src: "./img_source/CarBasic/products/red-car.jpg",
  };
  handleChangeColor = (color) => {
    let src = `./img_source/CarBasic/products/${color}-car.jpg`;
    this.setState({ img_src: src });
  };
  render() {
    return (
      <div>
        <h2>Ex_Car_Color</h2>
        <div className="row">
          <img className="col-4" src={this.state.img_src} />
          <div>
            <button
              onClick={() => {
                this.handleChangeColor("red");
              }}
              className="btn btn-danger"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("black");
              }}
              className="btn btn-danger mx-5"
            >
              Black
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("silver");
              }}
              className="btn btn-danger"
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}
