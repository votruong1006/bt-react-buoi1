import "./App.css";
import BaiTapLayoutComponent from "./BaiTapLayoutComponent/BaiTapLayoutComponent";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoClass />
      <DemoFunction></DemoFunction>
      <DemoFunction /> */}
      {/* <Ex_Layout /> */}
      {/* <DataBinding /> */}
      {/* <EventHandling /> */}
      {/* <ConditionalRendering /> */}
      {/* <DemoState /> */}
      {/* <RenderWithMap /> */}
      {/* <Ex_Car_Color /> */}
      <BaiTapLayoutComponent />
    </div>
  );
}

export default App;
