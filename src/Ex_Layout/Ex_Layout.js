import React, { Component } from 'react';
import Content from './Content';
import Footer from './Footer';
import Header from './Header';
import Navigate from './Navigate';

export default class Ex_Layout extends Component {
  render() {
    // jsx : html + js
    return (
      <div>
        <Header />
        <div className="row">
          <div className="col-6 p-0">
            <Navigate />
          </div>
          <div className="col-6 p-0">
            <Content />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
