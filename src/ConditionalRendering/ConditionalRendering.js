import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  isLogin = false;
  handleDangNhap = () => {
    console.log("trước", this.isLogin);
    this.isLogin = true;
    console.log("sau", this.isLogin);
  };
  handleDangXuat = () => {
    console.log("trước", this.isLogin);
    this.isLogin = false;
    console.log("sau", this.isLogin);
  };
  renderContentButton = () => {
    if (this.isLogin) {
      return (
        <button onClick={this.handleDangXuat} className="btn btn-danger">
          Đăng xuất{" "}
        </button>
      );
    } else {
      return (
        <button onClick={this.handleDangNhap} className="btn btn-danger">
          Đăng Nhập{" "}
        </button>
      );
    }
  };
  render() {
    return (
      <div>
        <h2>ConditionalRendering</h2>
        <div>{this.renderContentButton()}</div>
      </div>
    );
  }
}
